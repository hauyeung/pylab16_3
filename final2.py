#!/usr/local/bin/python3
#
#   final2.py
#
#   Ho Hang Au Yeung
#   Apr 21, 2014
#   Python 1m Lesson 16, Objective 3
#
#local imports
import sys
import os.path
#get word length
def wordlength(s):
	length = 0
	for x in s:
		if x.isalpha():
			length+=1
	return length
#check for file
if len(sys.argv)==2 and os.path.isfile(sys.argv[1]):
        path = sys.argv[1]	
        f = open(path, 'r')
        text = f.read()
        words = text.split()
        wlens = []

        #get word length and add to list
        for y in words:
                wlens.append(wordlength(y))

        lengraph = {}
        lencount = []
        print("{:<12}{:<5}".format("Length","Count"))
        #build dict of word length and frequency
        for z in range(1,max(wlens)+1,1):
                lengraph[z] = wlens.count(z)
                lencount.append(wlens.count(z))
                if (wlens.count(z) != 0):
                        print("{:<12}{:<5}".format(str(z),str(wlens.count(z))))
                        
        bars = []
        #print histogram
        for y in range(max(lencount) - max(lencount) %10 +10,0,-10):
                b = str(y)
                c='|'
                d=''
                for z,a in lengraph.items():                        
                                if (a >= y):
                                        d+='*   '
                                else:
                                        d+='    '
                #add vertical bars
                bars.append("{:>3}{:<1}{:<1}".format(b,c,d))

        print()

        for a in bars:
                print(a)

        l = ''
        nums = ''
        for k in range(1, max(wlens)+1,1):
                l += '----'
                if (k < 10):
                        nums += str(k)+'   '
                else:
                        nums+= str(k)+'  '
        #add bottom of graph
        bottom = "{:<3}{:<1}{:<1}".format('','|',nums)
        line = "{:<3}{:<1}{:<1}".format('','|',l)
        print(line)
        print(bottom)
else:
        print("Invalid file or argument")


		
	
